# Simple Models Analysis

[![pipeline status](https://gitlab.com/dsbowen/publication-bias/badges/master/pipeline.svg)](https://gitlab.com/dsbowen/publication-bias/-/commits/master)
[![License](https://img.shields.io/badge/License-MIT-brightgreen.svg)](https://gitlab.com/dsbowen/mega-analysis/-/blob/master/LICENSE)
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/dsbowen%2Fmega-analysis/HEAD?urlpath=lab/tree/examples)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)

Code available [here](https://gitlab.com/dsbowen/publication-bias).

## Environment

Click [here](https://gitpod.io/#https://gitlab.com/dsbowen/publication-bias/-/tree/master/) to run a pre-configured cloud environment with Gitpod.

If working locally, you'll need python-3.8 or higher. Set up your environment with:

```
$ pip install -r requirements.txt
```

## Data

To run the analyses starting from the processed data, download the data [here](https://osf.io/3pk4r/). Put the `data` folder in the root directory of this repository (i.e., in the same folder as this README file). See below for instructions on accessing external data.

## Cleaning

To re-run the cleaning code, download the external data files (see instructions below) and use:

```
$ make data
```

The resulting files will be in `data/processed`.

## Model predictions

```
$ make predict
```

The resulting files will be in `data/processed`.

## Analyses

```
$ make analyze
```

The resulting analyses will be in `results/analysis`.

## External data sources

All data from external sources should go in the `data/external` folder.

[Data](https://osf.io/fgjvw/) for the Reproducibility Project: Psychology (RPP). Make sure `rpp_data.csv` is UTF-8 encoded.

[Prediction market](https://osf.io/kn7f4/), [survey 1](https://osf.io/3nqd6/) and [survey 2](https://osf.io/qfuwe/) data for the RPP prediction market. Make sure `data4Rmerged_final_20151027.csv` is UTF-8 encoded. See the references in tables S1 and S2 in the [Reproducibility Project prediction study](https://www.pnas.org/doi/full/10.1073/pnas.1516179112) to match the study numbers in the survey data files to the studies numbers in `rpp_data.csv` (stored in `data/raw/rpp_prediction_study_matching.csv`).

[Data](https://osf.io/n8su7/?view_only=546ed2d8473f4978b95948a52712a3c5) for the Walmart flu megastudy predictions. Use the `Code & Data/Data files/WMT_PE_Clean.csv` file.

[Data](https://osf.io/9s6mc/?view_only=8bb9282111c24f81a19c2237e7d7eba3) for the exercise megastudy predictions. Use the `Data/Prediction Data/BE_Practitioners_Data.csv` (predictions), `Data/Regression Output/main_coef.csv` (study results) and `Data/SetupUp Data/pptdata.csv` (raw data) files. Finally, you'll need a file in the [Code](https://osf.io/k7rmb/?view_only=8bb9282111c24f81a19c2237e7d7eba3) folder `Code/Prediction Project/names.RDS` to map coded treatment values to intelligible treatment names.

[Data](https://www-journals-uchicago-edu.proxy.library.upenn.edu/doi/suppl/10.1086/699976#) for the effort megastudy predictions. Use the `DataForPosting/dtafiles/ExpertForecastCleanLong.dta` and `DataForPosting/input/MTurkCleanedDataShort.dta` files.

[Data](https://www.econometricsociety.org/publications/econometrica/2022/01/01/rcts-scale-comprehensive-evidence-two-nudge-units) for the RCTs to scale prediction study files. Use `cleaned_data/Forecasts.dta` and `cleaned_data/NudgeUnits.dta`.