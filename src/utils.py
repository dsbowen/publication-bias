"""Global variables and utilities.
"""
from __future__ import annotations

import os

import matplotlib.pyplot as plt

# paths to data files
DATA_DIR = "data"
RAW_DATA_DIR = os.path.join(DATA_DIR, "raw")
EXTERNAL_DATA_DIR = os.path.join(DATA_DIR, "external")
if not os.path.exists(INTERMEDIATE_DATA_DIR := os.path.join(DATA_DIR, "intermediate")):
    os.mkdir(INTERMEDIATE_DATA_DIR)
if not os.path.exists(PROCESSED_DATA_DIR := os.path.join(DATA_DIR, "processed")):
    os.mkdir(PROCESSED_DATA_DIR)
if not os.path.exists(EFFORT_DATA_DIR := os.path.join(PROCESSED_DATA_DIR, "effort")):
    os.mkdir(EFFORT_DATA_DIR)
if not os.path.exists(
    EXERCISE_DATA_DIR := os.path.join(PROCESSED_DATA_DIR, "exercise")
):
    os.mkdir(EXERCISE_DATA_DIR)
if not os.path.exists(FLU_DATA_DIR := os.path.join(PROCESSED_DATA_DIR, "flu")):
    os.mkdir(FLU_DATA_DIR)
if not os.path.exists(RCT_DATA_DIR := os.path.join(PROCESSED_DATA_DIR, "rct")):
    os.mkdir(RCT_DATA_DIR)
if not os.path.exists(
    REPRODUCIBILITY_DATA_DIR := os.path.join(PROCESSED_DATA_DIR, "reproducibility")
):
    os.mkdir(REPRODUCIBILITY_DATA_DIR)


# paths to simulation data
SIMULATIONS_DIR = "simulations"
RPP_PREDICTIONS_DATA = os.path.join(
    SIMULATIONS_DIR, "rpp_predictions", "data", "data.csv"
)

# paths to results directories
if not os.path.exists(RESULTS_DIR := "results"):
    os.mkdir(RESULTS_DIR)
if not os.path.exists(CLEANING_DIR := os.path.join(RESULTS_DIR, "make_data")):
    os.mkdir(CLEANING_DIR)
if not os.path.exists(ANALYSIS_DIR := os.path.join(RESULTS_DIR, "analysis")):
    os.mkdir(ANALYSIS_DIR)
if not os.path.exists(FIGURES_DIR := os.path.join(RESULTS_DIR, "figures")):
    os.mkdir(FIGURES_DIR)

EXPERIMENT_RESULTS = "experiment_results.csv"
FORECASTS = "expert_forecasts.csv"
PREDICTIONS = "model_predictions.csv"


def make_blank_figure(ax):
    """Create a blank axis with the same axes as ``ax``.

    Args:
        ax (AxesSubplot): Axis used to create the blank figure.

    Returns:
        AxesSubplot: Blank axis.
    """
    _, blank_ax = plt.subplots()
    blank_ax.set_xlim(ax.get_xlim())
    blank_ax.set_xticklabels(ax.get_xticklabels())
    blank_ax.set_xticks(ax.get_xticks())
    blank_ax.set_xlabel(ax.get_xlabel())
    blank_ax.set_ylim(ax.get_ylim())
    blank_ax.set_yticklabels(ax.get_yticklabels())
    blank_ax.set_yticks(ax.get_yticks())
    blank_ax.set_title(ax.get_title())
    blank_ax.set_ylabel(ax.get_ylabel())
    return blank_ax
