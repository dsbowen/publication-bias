"""Clean experiment results and expert forecasts.
"""
from __future__ import annotations

import os
import re

import numpy as np
import pandas as pd
import pyreadr
import statsmodels.api as sm
from multiple_inference.base import ModelBase
from multiple_inference.confidence_set import BaselineComparison
from linearmodels import PanelOLS
from scipy.stats import norm

from src.utils import (
    EXTERNAL_DATA_DIR,
    RAW_DATA_DIR,
    EFFORT_DATA_DIR,
    EXERCISE_DATA_DIR,
    FLU_DATA_DIR,
    RCT_DATA_DIR,
    REPRODUCIBILITY_DATA_DIR,
    EXPERIMENT_RESULTS,
    FORECASTS,
)

COLUMNS = [
    "treatment",
    "effect",
    "forecaster",
    "forecaster_pred",
    "model",
    "model_pred",
    "study",
]


def clean_flu() -> None:
    """Clean data from the Walmart flu megastudy."""
    df_pred = pd.read_csv(os.path.join(EXTERNAL_DATA_DIR, "WMT_PI_Clean.csv"))

    # clean experiment results
    df = df_pred.drop_duplicates("Condition")
    n_obs_per_condition = 689693 / len(df)  # 689693 is the total number of participants
    model = BaselineComparison(
        100 * df.PercVaccinated,
        (100**2)
        * np.diag(df.PercVaccinated * (1 - df.PercVaccinated) / n_obs_per_condition),
        exog_names=df.Condition,
        baseline="Control",
        sort=True,
    )
    model = ModelBase(
        model.mean,
        model.cov,
        exog_names=model.exog_names,
        endog_names="Increase in vaccination rate compared to control",
    )
    model.to_csv(os.path.join(FLU_DATA_DIR, EXPERIMENT_RESULTS))

    # clean expert forecasts
    df_pred["forecast"] = 100 * (df_pred.Prediction - df_pred.ControlPrediction)
    df_pred = df_pred[["Condition", "source", "forecast"]]
    df_pred = pd.pivot(df_pred, index="Condition", columns="source", values="forecast")
    # drop dtAO_5 because this forecaster consistently estimates the effects of the
    # messages are negative by around 70 percentage points, indicating confusion about
    # the task
    df_pred = df_pred.drop("Control").drop(columns="dtAO_5")
    df_pred.index = df_pred.index.rename("treatment")
    df_pred.loc[model.exog_names].to_csv(os.path.join(FLU_DATA_DIR, FORECASTS))


def clean_exercise() -> None:
    """Clean the data from the exercise megastudy."""
    # clean experiment results
    df = pd.read_csv(os.path.join(EXTERNAL_DATA_DIR, "pptdata.csv"))
    df = df.rename(columns={"Lc": "cohort_weeks", "Ngc": "cohort_size", "visits": "y"})
    # keep data from pre-intervention and intervention period
    # after weeks 4 is the post-intervention period
    df = df[(-52 <= df.week) & (df.week <= 4)]
    df["n_weeks"] = df.groupby("participant_id").week.transform("count")
    # indicates this observation is in the intervention period
    df["intervention_period"] = (1 <= df.week) & (df.week <= 4)
    df["cohort_week"] = pd.Categorical(df["cohort"] + "_" + df["week"].astype(str))
    df["cohort_week"] = df.cohort_week.cat.codes
    df = df.set_index(["participant_id", "cohort_week"])
    # get exogenous variables and fit model
    treatment_dummies = pd.get_dummies(df.exp_condition)
    X = treatment_dummies.apply(lambda x: x * df.intervention_period)
    X["Placebo Control"] = 1
    model = PanelOLS(
        df.y,
        X,
        weights=df.cohort_weeks / (df.n_weeks * df.cohort_size),
        entity_effects=True,
        time_effects=True,
    )
    results = model.fit(cov_type="clustered", cluster_entity=True, cluster_time=True)
    # save results
    model = ModelBase(
        results.params,
        results.cov,
        sort=True,
        columns=[col for col in results.params.index if col != "Placebo Control"],
        endog_names="Increase in weekly gym visits compared to control",
    )
    model.to_csv(os.path.join(EXERCISE_DATA_DIR, EXPERIMENT_RESULTS))

    # clean expert forecasts
    df = pd.read_csv(os.path.join(EXTERNAL_DATA_DIR, "BE_Practitioners_Data.csv"))
    df = pd.wide_to_long(
        df,
        ["prediction_weekly", "prediction_binary"],
        "ResponseId_Response ID",
        "treatment_arm",
        sep="_",
        suffix="\w+",
    ).reset_index()
    arm_names_df = pyreadr.read_r(os.path.join(EXTERNAL_DATA_DIR, "names.RDS"))[None]
    arm_names_df["treatment_arm"] = arm_names_df.treatment_arm.str.lower()
    df = df.merge(arm_names_df, on="treatment_arm")
    df = pd.pivot(
        df,
        index="final_label",
        columns="ResponseId_Response ID",
        values="prediction_weekly",
    )
    df.index = df.index.rename("treatment")
    df.loc[model.exog_names].to_csv(os.path.join(EXERCISE_DATA_DIR, FORECASTS))


def clean_effort() -> None:
    """Clean data from the effort study."""
    # clean experiment results
    df = pd.read_stata(os.path.join(EXTERNAL_DATA_DIR, "MTurkCleanedDataShort.dta"))
    results = sm.OLS(df.buttonpresses, pd.get_dummies(df.treatmentname)).fit(
        cov_type="HC3"
    )
    model = ModelBase.from_results(
        results,
        columns=[
            col
            for col in results.model.exog_names
            if col not in ("No Payment", "1c PieceRate", "10c PieceRate")
        ],
        sort=True,
    )
    model.to_csv(os.path.join(EFFORT_DATA_DIR, EXPERIMENT_RESULTS))

    # clean expert forecasts
    df_pred = pd.read_stata(
        os.path.join(EXTERNAL_DATA_DIR, "ExpertForecastCleanLong.dta")
    )
    df_pred = df_pred[df_pred.sampleshort == "experts"]
    df_pred = pd.pivot(df_pred, index="treatmentname", columns="id", values="forecast")
    df_pred.index = df_pred.index.rename("treatment")
    df_pred.loc[model.exog_names].to_csv(os.path.join(EFFORT_DATA_DIR, FORECASTS))


def clean_rct() -> None:
    """Clean data from the RCT study."""
    df = pd.read_stata(os.path.join(EXTERNAL_DATA_DIR, "NudgeUnits.dta"))
    df["trialnumber"] = "study_" + df.trialnumber.astype(str)
    df_pred = pd.read_stata(
        os.path.join(EXTERNAL_DATA_DIR, "Forecasts.dta"), convert_categoricals=False
    )
    # mappings given by personal correpondence with the authors
    forecasts_example_mapping = {
        1: 1502,
        2: 2017059,
        3: 1599.2,
        4: 2017027,
        5: 2017133,
        6: 2018076,
        7: 2015108,
        8: 2017073,
        9: 2016074,
        10: 2017150,
        11: 1515,
        12: 2017160,
        13: 2015148,
        14: 2016019,
    }
    df_pred["treatment"] = "study_" + df_pred.example.map(
        forecasts_example_mapping
    ).astype(str)

    # some of the trial numbers have multiple entries in the nudge results dataset
    # so you need to match not only by trial number but also by estimated treatment effect
    df = df.merge(
        df_pred.drop_duplicates("treatment"),
        left_on="trialnumber",
        right_on="treatment",
        how="left",
    )
    # "te" is the estimated treatment effect column in the forecasts dataset
    mask = ~df.te.isna() & (abs(df.te - df.treatmenteffect) > 0.01)
    df["trialnumber"].iloc[mask] = df.trialnumber[mask] + ".1"

    model = ModelBase(
        df.treatmenteffect,
        np.diag(df.SE**2),
        exog_names=df.trialnumber,
        endog_names="Percentage point increase in adoption",
        sort=True,
    )
    model.to_csv(os.path.join(RCT_DATA_DIR, EXPERIMENT_RESULTS))

    # get field experience groups
    df_exp = df_pred.drop_duplicates("id").set_index("id")
    df_exp = pd.get_dummies(df_exp.fieldexpgroup)[
        ["No field experience", "1-5 field experiments", ">5 field experiments"]
    ]
    df_exp.to_csv(os.path.join(RCT_DATA_DIR, "field_experience.csv"))

    # "fte" is the forecasted treatment effect
    df_pred = pd.pivot(df_pred, index="treatment", columns="id", values="fte")
    df_pred = df_pred.loc[[col for col in model.exog_names if col in df_pred.index]]
    df_pred.to_csv(os.path.join(RCT_DATA_DIR, FORECASTS))


def clean_reproducibility(alpha: float = 0.05) -> None:
    """Clean data from the reproducibility study.

    Args:
        alpha (float, optional): Significance level. Defaults to 0.05.
    """
    df = pd.read_csv(os.path.join(EXTERNAL_DATA_DIR, "rpp_data.csv"))

    # select columns required to estimate effects
    columns_map = {
        "Study Num": "study_num",
        "T_N..O.": "n_original",
        "T_pval_USE..O.": "pval_original",
        "T_N..R.": "n_replication",
        "T_pval_USE..R.": "pval_replication",
        "Direction (R)": "same_direction",
    }
    df = df[columns_map.keys()].rename(columns=columns_map)
    df["same_direction"] = df.same_direction == "same"
    df = df.dropna()
    df["study_num"] = "study_" + df.study_num.astype(str)

    # select studies that originally achieved a significant result
    df = df[df.pval_original < alpha]

    # estimate effects in replication studies
    # if the pvalue equals 0, the estimated effect will be infinte, so clip at .001
    df["pval_original"] = np.clip(df.pval_original, 0.001, 1)
    df["std_original"] = np.sqrt(1 / df.n_original)
    df["effect_original"] = norm.ppf(1 - df.pval_original / 2) * df.std_original

    df["pval_replication"] = np.clip(df.pval_replication, 0.001, 1)
    df["std_replication"] = np.sqrt(1 / df.n_replication)
    effect_same_direction = norm.ppf(1 - df.pval_replication / 2)
    effect_diff_direction = norm.ppf(df.pval_replication / 2)
    df["effect_replication"] = (
        df.same_direction * effect_same_direction
        + (1 - df.same_direction) * effect_diff_direction
    ) * df.std_replication

    df = df.sort_values("effect_replication", ascending=False)
    model = ModelBase(
        df.effect_replication,
        np.diag(df.std_replication**2),
        exog_names=df.study_num,
        endog_names="Replication effect",
    )
    model.to_csv(os.path.join(REPRODUCIBILITY_DATA_DIR, EXPERIMENT_RESULTS))
    df.to_csv(
        os.path.join(REPRODUCIBILITY_DATA_DIR, "experiment_results_long.csv"),
        index=False,
    )

    # clean expert forecasts
    study1_df = clean_forecast_study_data(
        pd.read_csv(os.path.join(EXTERNAL_DATA_DIR, "survey1anon.csv")), 1
    )
    study2_df = clean_forecast_study_data(
        pd.read_csv(os.path.join(EXTERNAL_DATA_DIR, "survey2anon.csv")), 2
    )
    # add 100 to be consistent with data4Rmerged_final_20151027 convention
    study2_df["study_num"] += 100
    df_pred = pd.concat((study1_df, study2_df))
    study_matching_df = pd.read_csv(
        os.path.join(RAW_DATA_DIR, "rpp_prediction_study_matching.csv")
    )
    df_pred = df_pred.merge(
        study_matching_df,
        left_on="study_num",
        right_on="rpp_prediction_study_no",
        how="outer",
    )
    df_pred["forecaster"] = pd.factorize(df_pred.forecaster)[0]
    df_pred["forecast"] = df_pred.forecast.astype(float) / 100
    df_pred = (
        df_pred[["rpp_study_no", "forecaster", "forecast"]]
        .rename(columns={"rpp_study_no": "study_num"})
        .dropna(subset="study_num")
    )
    df_pred = pd.pivot(
        df_pred, index="study_num", columns="forecaster", values="forecast"
    )
    df_pred.index = "study_" + df_pred.index.rename("treatment").astype(float).astype(
        str
    )
    exog_names = [name for name in model.exog_names if name in df_pred.index]
    df_pred.loc[exog_names].to_csv(os.path.join(REPRODUCIBILITY_DATA_DIR, FORECASTS))


def clean_forecast_study_data(df: pd.DataFrame, prediction_study: str) -> pd.DataFrame:
    """Clean the data from the first or second prediction market studies.

    Note that there were 2 prediction market studies. This function cleans the data from
    each individually.

    Args:
        df (pd.DataFrame): Survey results from the study.
        prediction_study (str): Name of the prediction study.

    Returns:
        pd.DataFrame: Cleaned survey results.
    """
    pattern = re.compile(r"q\d+a")
    df = df[[col for col in df.columns if pattern.match(col)]].rename(
        columns={col: col.rstrip("a") for col in df.columns}
    )
    df["forecaster"] = f"study{prediction_study}_" + df.index.astype(str)
    df = (
        pd.wide_to_long(df, "q", i="forecaster", j="study_num")
        .reset_index()
        .rename(columns={"q": "forecast"})
    )
    df = df[df.forecast != "."]
    return df


if __name__ == "__main__":
    clean_flu()
    clean_exercise()
    clean_effort()
    clean_rct()
    clean_reproducibility()
