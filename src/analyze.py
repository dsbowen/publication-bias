"""Compare forecaster and model predictions.
"""
from __future__ import annotations

import logging
import os

import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import seaborn as sns
import statsmodels.api as sm
from multiple_inference.bayes import Nonparametric, Normal
from multiple_inference.confidence_set import ConfidenceSet, PairwiseComparison
from multiple_inference.stats import joint_distribution
from scipy.stats import dirichlet, norm

from src.utils import (
    EFFORT_DATA_DIR,
    EXERCISE_DATA_DIR,
    EXPERIMENT_RESULTS,
    EXTERNAL_DATA_DIR,
    FLU_DATA_DIR,
    RCT_DATA_DIR,
    REPRODUCIBILITY_DATA_DIR,
    FORECASTS,
    PREDICTIONS,
    ANALYSIS_DIR,
    RESULTS_DIR,
)

squared_error = lambda predicted, actual: (predicted - actual) ** 2

logging.basicConfig(
    filename=os.path.join(ANALYSIS_DIR, "output.log"), filemode="w", level=logging.INFO
)


class Analyzer:
    """Compares forecaster and model predictions for a given study.

    Args:
        data_path (str): Path to the data.
        results_path (str): Path to the results.
        loss (function): Takes predicted and actual results and returns the loss.
        bayes_model (BayesModel): Bayesian model used to sample from the posterior distribution of effects.
        transform_sample_effects (function, optional): Transforms the sampled effects. Defaults to None.
        n_samples (int, optional): Number of samples to draw. Defaults to 10000.
    """

    def __init__(
        self,
        data_path,
        results_path,
        loss,
        bayes_model,
        transform_sample_effects=None,
        n_samples=10000,
    ):
        if not os.path.exists(results_path := os.path.join(ANALYSIS_DIR, results_path)):
            os.mkdir(results_path)
        self.results_path = results_path

        # read in the data
        self.df_forecast = pd.read_csv(os.path.join(data_path, FORECASTS)).set_index(
            "treatment"
        )
        df_pred = pd.read_csv(os.path.join(data_path, PREDICTIONS)).set_index(
            "treatment"
        )
        self.df_pred = df_pred.loc[self.df_forecast.index].T
        self.results = bayes_model.from_csv(
            os.path.join(data_path, EXPERIMENT_RESULTS), sort=True
        )

        # sample "true" effects from the posterior of a Bayesian model
        try:
            dist = self.results.get_joint_distribution(columns=self.df_forecast.index)
        except NotImplementedError:
            dist = joint_distribution(
                [
                    self.results.get_marginal_distribution(col)
                    for col in self.df_forecast.index
                ]
            )
        sample_effects = dist.rvs(n_samples)
        if transform_sample_effects is not None:
            sample_effects = transform_sample_effects(self, sample_effects)

        # reshape for broadcasting
        self.forecasts = np.expand_dims(self.df_forecast, (0, 1))
        self.model_pred = np.expand_dims(self.df_pred, (1, 3))
        self.oracle_pred = np.expand_dims(sample_effects.mean(axis=0), (0, 1, -1))
        self.sample_effects = np.expand_dims(sample_effects, (0, -1))

        # get weights for Bayesian bootstrap
        forecaster_weights = dirichlet.rvs(
            self.forecasts.shape[-1] * [1], size=n_samples
        )
        self.forecaster_weights = np.expand_dims(forecaster_weights, (0, 2))
        denom = (self.forecaster_weights * ~np.isnan(self.forecasts)).sum(axis=-1)
        self.denom = np.expand_dims(denom, -1)
        treatment_weights = dirichlet.rvs(self.forecasts.shape[2] * [1], size=n_samples)
        self.treatment_weights = np.expand_dims(treatment_weights, 0)

        # compute bias, risk, and comparative risk
        bias = (
            self.forecaster_weights * (self.forecasts - self.sample_effects)
        ) / self.denom
        self.bias = np.nansum(bias, axis=-1)
        self.bias_avg = (self.treatment_weights * self.bias).sum(axis=-1).squeeze()
        forecaster_risk = (
            self.forecaster_weights * loss(self.forecasts, self.sample_effects)
        ) / self.denom
        self.risk = {
            "forecaster": np.nansum(forecaster_risk, axis=-1),
            "model": loss(self.model_pred, self.sample_effects).squeeze(axis=-1),
            "oracle": loss(self.oracle_pred, self.sample_effects).squeeze(axis=-1),
        }
        self.risk_avg = pd.DataFrame(
            np.vstack(
                [(treatment_weights * r).sum(axis=-1) for r in self.risk.values()]
            ).T,
            columns=["Forecaster"]
            + [f"{x.capitalize()} model" for x in self.df_pred.index]
            + ["Oracle"],
        )
        comparative_risk = (
            self.forecaster_weights
            * (
                loss(self.forecasts, self.sample_effects)
                - loss(self.model_pred, self.sample_effects)
            )
        ) / self.denom
        self.comparative_risk = np.nansum(comparative_risk, axis=-1)
        self.comparative_risk_avg = pd.DataFrame(
            (self.treatment_weights * self.comparative_risk).sum(axis=-1),
            index=self.df_pred.index,
        )

    def analyze_risk(self, alpha=0.05):
        """Compare forecasters to models in terms of risk.

        Args:
            alpha (float, optional): Significance level. Defaults to 0.05.
        """
        logging.info("Analyzing risk")
        logging.info(
            "Analyzing average comparative risk using the posterior distribution"
        )
        self.describe(self.comparative_risk_avg)

        if self.comparative_risk_avg.shape[0] > 1:
            # correct for multiple comparisons (i.e., comparing forecasters to multiple models)
            results = ConfidenceSet(
                self.comparative_risk_avg.T.mean(), self.comparative_risk_avg.T.cov()
            ).fit()
            logging.info(
                "Analyzing average comparative risk using simultaneous confidence sets"
            )
            logging.info(results.summary())
            logging.info(results.test_hypotheses())

        # plot the risk for forecasters, models, and the oracle
        y = np.arange(0, -self.risk_avg.shape[-1], -1)
        fig, ax = plt.subplots()
        points = self.risk_avg.mean()
        quantiles = self.risk_avg.quantile([alpha / 2, 1 - alpha / 2])
        logging.info(f"Average risk for forecasters, models, and the oracle: {points}")
        logging.info(f"95% CI: {quantiles}")
        ax.errorbar(
            points,
            y,
            xerr=[points - quantiles.iloc[0], quantiles.iloc[1] - points],
            fmt="o",
        )
        ax.set_yticks(y)
        ax.set_yticklabels(self.risk_avg.columns)
        ax.set_xlabel("Risk")
        fig.savefig(
            os.path.join(self.results_path, "risk.png"), bbox_inches="tight", dpi=300
        )

    def summarize_mean_effect(self):
        """Print the average effect across all treatments."""
        ones = np.ones((len(self.results.mean), 1))
        cov_inv = np.linalg.inv(self.results.cov)
        mean_effect = (
            np.linalg.inv(ones.T @ cov_inv @ ones)
            @ ones.T
            @ cov_inv
            @ self.results.mean
        )
        logging.info(f"Average effect: {mean_effect}")

    def analyze_bias(self, bias=None):
        """Estimate the probability that forecasters are biased.

        Args:
            bias (pd.DataFrame, optional): Bias results to use instead of ``self.bias``. Defaults to None.
        """
        logging.info("Analyzing bias")
        exog_names = None
        if bias is None:
            bias = self.bias.squeeze()
            exog_names = self.df_forecast.index
            bias_avg = self.bias_avg
        else:
            treatment_weights = dirichlet.rvs(bias.shape[1] * [1], size=bias.shape[0])
            bias_avg = (treatment_weights * bias.values).sum(axis=-1)

        logging.info("Analyzing average bias using the posterior distribution")
        self.describe(bias_avg)

        # analyze bias for each treatment
        results = ConfidenceSet(
            bias.mean(axis=0),
            np.cov(bias.T),
            exog_names=exog_names,
            endog_names="Bias",
            sort=True,
        ).fit()
        logging.info(
            "Analyzing bias for individual treatments using simultaneous confidence sets"
        )
        logging.info(results.summary())
        logging.info(results.test_hypotheses())
        ax = results.point_plot()
        ax.axvline(0, linestyle="--")
        plt.savefig(
            os.path.join(self.results_path, "bias.png"), bbox_inches="tight", dpi=300
        )

    def describe(self, arr, alpha=0.05):
        """Describe an array (mean, ``1-alpha`` confidence interval, and CDF at 0).

        Args:
            arr (np.ndarray): Array.
            alpha (float, optional): Significance level. Defaults to 0.05.
        """
        arr = np.array(arr)
        logging.info(f"Mean: {arr.mean(axis=-1)}")
        logging.info(f"95% CI: {np.quantile(arr, [alpha/2, 1-alpha/2], axis=-1)}")
        logging.info(f"Probability value < 0: {(arr < 0).mean(axis=-1)}. You may have to double this to approximate a 2-tailed test.")


def analyze_flu() -> None:
    analyzer = Analyzer(FLU_DATA_DIR, "flu", squared_error, Normal)
    analyzer.summarize_mean_effect()
    analyzer.analyze_bias()
    analyzer.analyze_risk()


def analyze_exercise() -> None:
    analyzer = Analyzer(EXERCISE_DATA_DIR, "exercise", squared_error, Normal)
    analyzer.summarize_mean_effect()
    analyzer.analyze_bias()
    analyzer.analyze_risk()


def analyze_effort() -> None:
    analyzer = Analyzer(EFFORT_DATA_DIR, "effort", squared_error, Normal)
    bias_df = pd.DataFrame(analyzer.bias.squeeze(), columns=analyzer.results.exog_names)
    # group treatments according to the categories in DellaVigna and Pope
    bias_df = pd.DataFrame(
        {
            "Framing": bias_df["Loss 40c"] - bias_df["Gain 40c"],
            "Crowding out": -bias_df["Very Low Pay"],
            "Social preferences": bias_df[
                ["10c RedCross", "1c RedCross", "Gift Exchange"]
            ].mean(axis=1),
            "Time discounting": -bias_df[["1c 2Wks", "1c 4Wks"]].mean(axis=1),
            "Risk aversion": -bias_df["Prob.5 2c"],
            "Social comparison": bias_df["Social Comp"],
            "Ranking": bias_df["Ranking"],
            "Task significance": bias_df["Task Signif"],
        }
    )
    analyzer.analyze_bias(bias_df)
    analyzer.analyze_risk()


def analyze_rct() -> None:
    def compute_bias_by_group(group):
        bias_group = bias[:, df[group]]
        forecaster_weights = dirichlet(bias_group.shape[1] * [1]).rvs(
            bias_group.shape[0]
        )
        return (forecaster_weights * bias_group).sum(axis=-1)

    analyzer = Analyzer(RCT_DATA_DIR, "rct", squared_error, Nonparametric)
    logging.info(f"Mean effect {analyzer.sample_effects.mean()}")
    analyzer.analyze_bias()
    analyzer.analyze_risk()

    treatment_weights = np.expand_dims(analyzer.treatment_weights, -1)
    denom = (treatment_weights * ~np.isnan(analyzer.forecasts)).sum(axis=2)
    denom = np.expand_dims(denom, axis=2)
    bias = treatment_weights * (analyzer.forecasts - analyzer.sample_effects) / denom
    bias = np.nansum(bias, axis=2).squeeze()
    df = (
        pd.read_csv(os.path.join(RCT_DATA_DIR, "field_experience.csv"))
        .set_index("id")
        .astype(bool)
    )
    bias_df = pd.DataFrame({col: compute_bias_by_group(col) for col in df.columns})

    mean = bias_df.mean()
    cov = bias_df.cov()
    cs_results = ConfidenceSet(
        mean,
        cov,
        sort=True,
        endog_names="Bias (percentage point adoption of target behavior)",
    ).fit()
    logging.info("Analyzing bias by experience group")
    logging.info(cs_results.summary())
    ax = cs_results.point_plot()
    ax.axvline(0, linestyle="--")
    plt.savefig(
        os.path.join(analyzer.results_path, "bias_by_experience.png"),
        bbox_inches="tight",
        dpi=300,
    )

    logging.info("Comparing experience groups to one another")
    logging.info(PairwiseComparison(mean, cov, sort=True).fit().test_hypotheses())


def analyze_reproducibility() -> None:
    def transform_sample_effects(analyzer, sample_effects, alpha=0.05):
        # the sample effects are the estimated treatment effects
        # we need to transform these into the probability that the study will replicate
        df = pd.read_csv(
            os.path.join(REPRODUCIBILITY_DATA_DIR, "experiment_results_long.csv")
        ).set_index("study_num")
        df = df.loc[analyzer.df_forecast.index]
        n_replication = np.expand_dims(df.n_replication, 0)
        return norm.cdf(
            np.sqrt(n_replication) * sample_effects - norm.ppf(1 - alpha / 2)
        )

    brier_score = lambda pred, actual: actual * ((1 - pred) ** 2) + (1 - actual) * (
        pred**2
    )
    analyzer = Analyzer(
        REPRODUCIBILITY_DATA_DIR,
        "reproducibility",
        brier_score,
        Nonparametric,
        transform_sample_effects,
    )
    logging.info(
        f"Average probability of reproducing: {analyzer.sample_effects.mean()}"
    )
    analyzer.analyze_bias()
    analyzer.analyze_risk()


if __name__ == "__main__":
    np.random.seed(0)
    sns.set()
    logging.info("ANALYZING EFFORT STUDY")
    analyze_effort()
    logging.info("ANALYZING EXERCISE STUDY")
    analyze_exercise()
    logging.info("ANALYZING FLU STUDY")
    analyze_flu()
    logging.info("ANALYZING RCT STUDY")
    analyze_rct()
    logging.info("ANALYZING REPRODUCIBILITY STUDY")
    analyze_reproducibility()
