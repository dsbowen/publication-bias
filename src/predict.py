"""Compute model predictions.
"""
from __future__ import annotations

import os

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
import statsmodels.api as sm
from scipy.stats import norm, multivariate_normal

from src.utils import (
    EFFORT_DATA_DIR,
    EXERCISE_DATA_DIR,
    EXPERIMENT_RESULTS,
    RCT_DATA_DIR,
    REPRODUCIBILITY_DATA_DIR,
    FORECASTS,
    FLU_DATA_DIR,
    PREDICTIONS,
    RESULTS_DIR,
)

if not os.path.exists(RESULTS_DIR := os.path.join(RESULTS_DIR, "predict")):
    os.mkdir(RESULTS_DIR)


def predict_flu():
    """Create null model predictions for the flu study."""
    df = pd.read_csv(os.path.join(FLU_DATA_DIR, FORECASTS))
    df = df.set_index("treatment")
    predictions = pd.Series(0, name="null", index=df.index)
    predictions.to_csv(os.path.join(FLU_DATA_DIR, PREDICTIONS))


def predict_exercise():
    """Create null model predictions for the exercise study."""
    df = pd.read_csv(os.path.join(EXERCISE_DATA_DIR, FORECASTS))
    df = df.set_index("treatment")
    predictions = pd.Series(0, name="null", index=df.index)
    predictions.to_csv(os.path.join(EXERCISE_DATA_DIR, PREDICTIONS))


def predict_effort():
    """Create linear interpolation predictions for the effort study."""
    # the researchers showed these results to the forecasters
    piecerates, points = [0, 0.01, 0.1], [1521, 2029, 2175]
    predict = lambda x: np.interp(x, piecerates, points)

    # get predictions
    selfish_predictions = {
        "Gain 80c": 2000,  # participants gained 80c if they reached 2000 presses
        "Loss 40c": 2000,
        "Gain 40c": 2000,
        "4c PieceRate": predict(0.04),
        "1c 2Wks": predict(0.01),
        "Prob.5 2c": predict(0.5 * 0.02),
        "1c 4Wks": predict(0.01),
        "10c RedCross": predict(0),
        "1c RedCross": predict(0),
        "Prob.01 $1": predict(0.01 * 1),
        "Very Low Pay": predict(0.001),
        "Social Comp": predict(0),
        "Ranking": predict(0),
        "Task Signif": predict(0),
        "Gift Exchange": predict(0),
    }
    # altrustic model assumes participants are just as motivated to earn money for the
    # Red Cross as they would be to earn money for themselves
    altruistic_predictions = selfish_predictions.copy()
    altruistic_predictions["10c RedCross"] = predict(0.1)
    altruistic_predictions["1c RedCross"] = predict(0.01)
    predictions = (
        pd.DataFrame(
            [selfish_predictions, altruistic_predictions],
            index=["selfish", "altruistic"],
        )
        .round()
        .T
    )
    predictions.index = predictions.index.rename("treatment")
    predictions.to_csv(os.path.join(EFFORT_DATA_DIR, PREDICTIONS))

    # plot interpolation
    x = np.linspace(min(piecerates), max(piecerates))
    ax = sns.lineplot(x=x, y=predict(x))
    plt.scatter(
        x=piecerates,
        y=points,
        marker="x",
        color=sns.color_palette()[1],
    )
    ax.set_ylabel("Interpolation model prediction")
    ax.set_xlabel("Expected piecerate (cents per 100 points)")
    plt.savefig(os.path.join(RESULTS_DIR, "interpolation.png"))


def predict_rct():
    """Create null model predictions for the RCT study."""
    df = pd.read_csv(os.path.join(RCT_DATA_DIR, EXPERIMENT_RESULTS))
    df = df.set_index(df.columns[0])
    predictions = pd.Series(0, name="null", index=df.columns.rename("treatment"))
    predictions.to_csv(os.path.join(RCT_DATA_DIR, PREDICTIONS))


def predict_reproducibility(alpha: float = 0.05):
    """Create null, random, and linear model predictions for the reproducibility study.

    Args:
        alpha (float, optional): Significance level. Defaults to 0.05.
    """

    def sample_variance(sq_error):
        # sample a variance parameter
        results = sm.OLS(sq_error, X).fit(cov_type="HC3")
        beta = multivariate_normal.rvs(results.params, results.cov_params())
        return np.clip(X @ beta, sq_error.min(), np.inf)

    df_results = pd.read_csv(
        os.path.join(REPRODUCIBILITY_DATA_DIR, "experiment_results_long.csv")
    )

    # fit linear regression model and predict replication probabiliy using Gibbs sampling
    results = sm.OLS(
        df_results.effect_replication, X := sm.add_constant(df_results.effect_original)
    ).fit(cov_type="HC3")
    beta = multivariate_normal.rvs(results.params, results.cov_params(), size=10000)
    effect_pred = (beta @ X.T).values
    sq_error = (np.expand_dims(df_results.effect_replication, 0) - effect_pred) ** 2
    variance = np.array([sample_variance(sq_error_i) for sq_error_i in sq_error])
    sample_effects = lambda loc, variance: multivariate_normal.rvs(
        loc, np.diag(variance)
    )
    effect_sample = np.array(
        [
            sample_effects(effect_pred_i, variance_i)
            for effect_pred_i, variance_i in zip(effect_pred, variance)
        ]
    )
    n_replication = np.expand_dims(df_results.n_replication, 0)
    pr_replicate = norm.cdf(
        np.sqrt(n_replication) * effect_sample - norm.ppf(1 - alpha / 2)
    )
    pr_replicate = pr_replicate.mean(axis=0)

    df = pd.DataFrame(
        {"null": [alpha / 2], "random": [0.5], "linear regression": pr_replicate},
        index=df_results.study_num,
    )
    df.index = df.index.rename("treatment")
    df.to_csv(os.path.join(REPRODUCIBILITY_DATA_DIR, PREDICTIONS))


if __name__ == "__main__":
    np.random.seed(0)
    sns.set()
    predict_flu()
    predict_exercise()
    predict_effort()
    predict_rct()
    predict_reproducibility()
